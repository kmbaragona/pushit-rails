class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name, limit: 100, null: false
      t.string :password, null: false

      t.timestamps null: false
    end
  end
end
