class CreateDomains < ActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.string :domain_name, index: true, unique: true
      t.references :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
