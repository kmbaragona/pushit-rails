class AddCreatorIpToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :creator_ip, :string
  end
end
