class AddIndexFilenameToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :index_filename, :string
  end
end
