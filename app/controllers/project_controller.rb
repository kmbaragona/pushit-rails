class ProjectController < ApiController

  def new_project
    desired_name = params[:desiredName]

    password = SecureRandom.uuid
    given_name = desired_name.downcase.gsub(/[\W|_]+/, "_")+((rand*100000).floor).to_s

    p = Project.create!(
      name: given_name,
      password: password,
      creator_ip: request.remote_ip
    )
    render :json => {
      name: p.name,
      password: p.password
    }
  end

  def getPathForProjectName(name)
    return Rails.application.config.MyConfig['project_files_path']+'/'+name+'/'
  end

  def getProjectWithNamePassword(name, password)
    project = Project.where(name: name).first
    if(project.password != password)
      raise 'wrong password'
    end
    return project
  end

  def add_file
    project = getProjectWithNamePassword(params[:name], params[:password])

    dest = params[:dest]
    if dest.match(/\.\.\//)
      raise 'dest path contains ".." will not upload'
    end

    final_path = getPathForProjectName(project.name)+dest;
    FileUtils.mkdir_p(File.dirname(final_path));
    FileUtils.mv(params[:the_file].tempfile.path, final_path);
    render :plain => "YES!"
  end

  def mkdir
    project = getProjectWithNamePassword(params[:name], params[:password])
    dest = params[:dest]
    if dest.match(/\.\.\//)
      raise 'dest path contains ".." will not upload'
    end

    final_path = getPathForProjectName(project.name)+dest;
    FileUtils.mkdir_p(final_path)
    render :plain => "YES!"
  end

  def getViewURLForProjectName
    url = 'http://'+params[:name]+'.'+request.headers["HTTP_HOST"]
    render :plain => url
  end

  def get_file

    subdomain = params[:project_id]
    request_path = params[:path]

    #ensure project exists
    project = Project.find_by!(name: subdomain)


    if(request_path.match(/\/$/))
      request_path = request_path.chop #ends with /
    end

    if request_path.length == 0
      # requesting index page of the entire project
      # only do this if the project has an index page
      if not project.index_filename.blank?
        request_path = '/'+project.index_filename
      end

    end

    local_path = getPathForProjectName(subdomain).chop + request_path
    if not File.exist?(local_path)
      #byebug
      render :plain => 'Does not exist', status: 404
      return
    end

    if File.file?(local_path)
      ext = File.extname(local_path)
      if ext.blank?
        type = 'text/plain'
      else
        type = Rack::Mime.mime_type(ext)
      end

      send_data File.read(local_path),
        :type => type,
        disposition: 'inline'
      return
    end
    if File.directory?(local_path)
      render "directory", :locals => {
          files: Dir.entries(local_path),
          current_dir: request_path,
          project: project
      },
      layout: 'application'
      return
    end


    render :plain => local_path
  end

end
