class ApiController < ActionController::Base
  skip_before_filter :verify_authenticity_token
  rescue_from Exception do |e|
    Rails.logger.error e
    render json: {exception: "#{e}"}, status: 500
  end
end
