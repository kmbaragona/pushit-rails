require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module PushitRails
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.middleware.insert 0, 'Rack::Rewrite' do
      rewrite %r{(.+)},
      lambda { |match, rack_env|
        new_url = match[1] # the entire thing
        host = rack_env['HTTP_HOST']

        get_new_url_for_project_name = lambda{ |old_url, project_name|
          old_url_parsed = URI.parse(old_url)
          new_url = "/project_files/#{project_name}?path="+Rack::Utils.escape(old_url_parsed.path)
          if not old_url_parsed.query.blank?
            new_url = new_url+'&'+old_url_parsed.query
          end
          return new_url
        }

        domain_obj = Domain.where(domain_name: host).first
        if domain_obj
          project = domain_obj.project
          new_url = get_new_url_for_project_name.call(new_url, project.name)
        else
          m = host.match(/^(\w+)\.#{Regexp.escape(Rails.application.config.MyConfig['hostname'])}/)
          if m
            subdomain = m[1];
            new_url = get_new_url_for_project_name.call(new_url, subdomain)
          end
        end

        new_url
      }

    end

    config.autoload_paths << Rails.root.join('lib')

  end
end
